package edu.binghamton.cs.hello;

import java.util.Vector;
import java.util.Random;

/**
 * Computer player is the player class for the AI
 * It has three difficulties in how it plays
 * They are described in there move sections
 *
 * @author Matthew Williamson
 */
public class ComputerPlayer implements Player {
	private final String name;
	private final int diff;
	private Trie dict;

	/**
	 * Constructor for the computer player
	 *
	 * @param number The number computer it is
	 * @param diff   The difficulty of the computer 1, 2, or 3
	 * @param dict   The dictionary for the game
	 */
	public ComputerPlayer(int number, int diff, Trie dict) {
		this.name = "Computer " + Integer.toString(number);
		if (diff < 1 || 3 < diff) {
			assert false : diff;
		}
		this.diff = diff;
		this.dict = dict;
	}

	/**
	 * Getter style method to choose the name of the player
	 *
	 * @return The name
	 */
	public String name() {
		return this.name;
	}

	/**
	 * Play is a strategy pattern for the different difficulties of the computer
	 *
	 * @param word Current word being created in the game
	 * @return     The letter choice made by the player
	 */
	public char play(String word) {
		char next = '\0';
		switch(this.diff) {
			case 1:
				next = easyPlay(word);
				break;
			case 2:
				next = mediumPlay(word);
				break;
			case 3:
				next = hardPlay(word);
				break;
			default:
				assert false : this.diff;
		}
		if (word.length() == 0) {
			System.out.println(this.name + " pick the first letter: " + next);
		}
		else {
			System.out.println(this.name + " pick the next letter: " + word + next);
		}
		return next;
	}

	/**
	 * Easy play method randomily chooses a letter that exists
	 * No strategy, simply picks viable letters
	 *
	 * @param word Current word being created in the game
	 * @return     The letter choice made by the player
	 */
	private char easyPlay(String word) {
		Vector<Trie> list = new Vector<>(this.dict.node(word).children.values());
		Random rand = new Random();
		return list.get(rand.nextInt(list.size())).c;
	}

	/**
	 * Medium play method tries to choose a letter such that it will keep the game going the longest
	 * If there is more than one it randomily chooses among them
	 *
	 * @param word Current word being created in the game
	 * @return     The letter choice made by the player
	 */
	private char mediumPlay(String word) {
		return longest(this.dict.node(word)).c;
	}

	/**
	 * Hard play method plays optimally
	 * If there is more than one it randomily chooses among them
	 * If it can't guarantee a victory it will play longest hoping you mess up
	 *
	 * @param word Current word being created in the game
	 * @return     The letter choice made by the player
	 */
	private char hardPlay(String word) {
		char next = '\0';
		Trie t = this.dict.node(word);
		Trie win = forceWin(t);
		if (win != null) {
			next = win.c;
		}
		else {
			next = longest(t).c;
		}
		return next;
	}

	/**
	 * Returns the child trie node which forces a win
	 *
	 * @param t The starting parent trie
	 * @return  The trie which forces a win
	 */
	private Trie forceWin(Trie t) {
		Trie best = null;
		for (Trie child : t.children.values()) {
			if (!child.isLeaf) {
				best = child;
				for (Trie grandChild : child.children.values()) {
					if (!grandChild.isLeaf && forceWin(grandChild) == null) {
						best = null;
						break;
					}
				}
			}
			if (best != null) {
				break;
			}
		}
		return best;
	}

	/**
	 * Returns the child trie node with the longest children
	 *
	 * @param t The starting parent trie
	 * @return  The trie with the longest children
	 */
	private Trie longest(Trie t) {
		Vector<Trie> children = new Vector<>(t.children.values());
		Trie max = children.get(0);
		for (Trie child : children) {
			if (child.maxLength() > max.maxLength()) {
				max = child;
			}
		}
		return max;
	}
}

