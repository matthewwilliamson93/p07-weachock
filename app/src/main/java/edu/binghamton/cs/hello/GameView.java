package edu.binghamton.cs.hello;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CopyOnWriteArrayList;

public class GameView extends View {
    final static float LOG_POP_SPEED = 10.0f;
    final static float LOG_POP_START = 80.0f;
    final static float LOG_POP_END = 40.0f;
    final static float LOG_SPACING = 160.0f;
    final static float LOG_START_POS = 800.0f;
    final static float KEYS_START_POS = LOG_START_POS + LOG_SPACING * 2;
    final static int CPU_THINK_SLOW = 80;
    final static int CPU_THINK_FAST = 10;

    final static int STATE_ADD_PLAYER = 0;
    final static int STATE_ADD_NAME = 1;
    final static int STATE_PLAY_CPU = 2;
    final static int STATE_PLAY_HUM = 3;
    final static int STATE_PLAY = 4;
    final static int STATE_START = 5;
    final static int STATE_NEXT = 6;

    Timer timer;
    Context context;
    ResourceManager res;
    Random rng;
    CopyOnWriteArrayList<FontSprite> log;
    CopyOnWriteArrayList<FontSprite> keys;
    CopyOnWriteArrayList<Player> players;
    FontSprite add_hum;
    FontSprite add_cpu;
    FontSprite name;
    String word;
    int ticks;
    int state;
    int turn;
    Trie dict;

    public GameView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);

        // initialize global state
        this.context = context;
        this.res = new ResourceManager(context);
        this.rng = new Random();
        this.dict = new Trie();
        this.keys = new CopyOnWriteArrayList<>();
        this.log = new CopyOnWriteArrayList<>();
        this.add_hum = new FontSprite(this.context, "Human");
        this.add_cpu = new FontSprite(this.context, "CPU");
        this.name = new FontSprite(this.context, "");
        this.add_hum.move(0, KEYS_START_POS);
        this.add_cpu.move(0, KEYS_START_POS + LOG_SPACING);
        this.name.move(0, KEYS_START_POS - LOG_SPACING);
        this.reset();

        for(char a='a'; a<='z'; a++) {
            FontSprite f = new FontSprite(this.context, "" + a);
            int i = (a - 'a');
            if(i < 21) {
                int x = i % 7;
                int y = i / 7;
                f.move(x * LOG_SPACING + LOG_SPACING/2.0f, KEYS_START_POS + y * LOG_SPACING);
            }
            else {
                int x = (i - 21);
                f.move(LOG_SPACING * 1.5f + x * LOG_SPACING, KEYS_START_POS + 3 * LOG_SPACING);
            }
            f.center();
            this.keys.add(f);
        }

        // populate the dict
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(context.getAssets().open("dict.txt")));
            String word;
            while((word = br.readLine()) != null) {
                this.dict.insert(word);
            }
            br.close();
        }
        catch(Exception e) {
            Log.e("BU", "FileNotFound exception.");
        }

        // set up interval timer
        this.timer = new Timer();
        this.timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                GameView.this.tick();
                GameView.this.postInvalidate();
            }
        }, 0, 16);
    }

    void reset() {
        this.ticks = 0;
        this.players = new CopyOnWriteArrayList<>();
        this.change(STATE_ADD_PLAYER);
    }

    void change(int new_state) {
        this.state = new_state;
        if(this.state == STATE_ADD_PLAYER) {
            this.add_line("Add players!");
            if(this.players.size() > 1) {
                this.name.text("Play!");
            }
            else {
                this.name.text("");
            }
        }
        else if(this.state == STATE_ADD_NAME) {
            this.add_line("What name?");
            this.name.text("");
        }
        else if(this.state == STATE_START) {
            this.add_line("Round start!");
            this.turn = this.rng.nextInt(this.players.size());
            this.word = "";
            this.change(STATE_PLAY);
        }
        else if(this.state == STATE_NEXT) {
            this.turn = (this.turn + 1) % this.players.size();
            this.change(STATE_PLAY);
        }
        else if(this.state == STATE_PLAY) {
            Player p = this.players.get(this.turn);
            this.name.text(p.name() + "...");
            if(p instanceof ComputerPlayer) {
                this.change(STATE_PLAY_CPU);
            }
            else {
                this.change(STATE_PLAY_HUM);
            }
        }
    }

    void tick() {
        this.ticks++;
        for(FontSprite f : this.log) {
            if(f.size() > LOG_POP_END + 1.0f) {
                float ds = f.size() - LOG_POP_END;
                f.size(f.size() - ds/LOG_POP_SPEED);
            }
        }
        if(this.state == STATE_PLAY_CPU) {
            int time = CPU_THINK_FAST;
            for(Player p : this.players) {
                if(p instanceof HumanPlayer) {
                    time = CPU_THINK_SLOW;
                }
            }
            if((this.ticks % time) == 0) {
                this.play(this.players.get(this.turn).play(this.word));
            }
        }
    }

    void add_line(String line) {
        for(FontSprite f : this.log) {
            f.dy(-LOG_SPACING);
        }

        FontSprite f = new FontSprite(this.context, line);
        f.move(0, LOG_START_POS);
        f.size(LOG_POP_START);
        this.log.add(f);
    }

    void type(char letter) {
        if(this.state == STATE_ADD_NAME) {
            this.name.text(this.name.text() + letter);
        }
        else {
            this.play(letter);
        }
    }

    void play(char letter) {
        this.word += letter;
        this.add_line(this.word);
        if(this.dict.contains(this.word)) {
            this.add_line("Out! Complete word.");
        }
        else if(!this.dict.isPrefix(this.word)) {
            this.add_line("Out! Not a prefix.");
        }
        else {
            this.change(STATE_NEXT);
            return;
        }

        this.players.remove(this.turn);
        if(this.players.size() == 1) {
            this.add_line(this.players.get(0).name() + " wins!");
            this.reset();
        }
        else {
            this.change(STATE_START);
        }
    }

    void press(float x, float y) {
        if(this.state == STATE_PLAY_HUM || this.state == STATE_ADD_NAME) {
            for(FontSprite key : this.keys) {
                RectF bounds = new RectF(key.x() - LOG_SPACING/2, key.y() - LOG_SPACING, key.x() + LOG_SPACING/2, key.y());

                if(bounds.contains(x, y)) {
                    this.type(key.text().charAt(0));
                    return;
                }
            }
        }
        if(this.state == STATE_ADD_PLAYER) {
            if(y > this.add_hum.y() - LOG_SPACING && y < this.add_hum.y()) {
                this.change(STATE_ADD_NAME);
            }
            else if(y > this.add_cpu.y() - LOG_SPACING && y < this.add_cpu.y()) {
                this.add_line("Added a CPU!");
                this.players.add(new ComputerPlayer(this.players.size()+1, this.rng.nextInt(2)+1, this.dict));
                if(this.players.size() > 1) {
                    this.name.text("Play!");
                }
            }
            else if(this.players.size() > 1 && y > this.name.y() - LOG_SPACING && y < this.name.y()) {
                this.change(STATE_START);
            }
        }
        else if(this.state == STATE_ADD_NAME) {
            if(this.name.text().length() > 0 && y > this.name.y() - LOG_SPACING && y < this.name.y()) {
                this.add_line("Added " + this.name.text() + "!");
                this.players.add(new HumanPlayer(this.name.text()));
                this.change(STATE_ADD_PLAYER);
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getAction() & MotionEvent.ACTION_MASK;
        if (action == MotionEvent.ACTION_DOWN) {
            MotionEvent me = (MotionEvent)event;
            this.press(me.getX(), me.getY());
        }
        return true;
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        for (Sprite b : this.log) {
            b.draw(canvas);
        }
        this.name.draw(canvas);
        if(this.state == STATE_ADD_PLAYER) {
            this.add_cpu.draw(canvas);
            this.add_hum.draw(canvas);
        }
        if(this.state == STATE_PLAY_HUM || this.state == STATE_ADD_NAME) {
            for (Sprite b : this.keys) {
                b.draw(canvas);
            }
        }
    }
}
