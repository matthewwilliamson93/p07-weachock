package edu.binghamton.cs.hello;

/**
 * Player is the interface for player types
 *
 * @author Matthew Williamson
 */
public interface Player {
	/**
	 * Getter style method to choose the name of the player
	 *
	 * @return The name
	 */
	String name();

	/**
	 * Play acts as the method in which a player chooses a letter
	 *
	 * @param word Current word being created in the game
	 * @return     The letter choice made by the player
	 */
	char play(String word);
}

